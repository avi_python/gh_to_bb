<%inherit file='base.mako' />
<div class="container">
    <div class="col-md-12 text-center">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 text-center" id="cont-pqs"><span>Process. Quality. Support.</span></div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1" id="cont-offer-text">Group of colleagues with 10+ years experience in software development industry joined together to build own company as the next step of our own growth.</div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1" id="cont-offer2-text">We offer our experience brought from different projects we run for big software development companies and own products developed meanwhile. While having stable core team we are welcome to cooperate on project based requirements with experts from other related areas to provide solid all-in-box services for our clients.</div>
        </div>
    </div>
</div>
<div class="col-md-12 tech-cont">
    <div class="container">
        <div class="col-md-10 col-md-offset-1 text-center" id="hww-text"><span>
            We are focused on particular directions to provide most effective price-quality services
            <b>Though we are open to go with other technologies set per project need</b></span>
        </div>
        <div class="row">
            <div class="col-md-12 wwd-cont">
                <div class="col-md-3 text-center">
                    <img width="100" height="100" src="${request.static_url("cdev:static/imgs/ic_desktop_mac_white_48dp_2x.png")}" alt="cdev"/>
                    <span class="wwd-title">desktop</span>
                    <span class="wwd-subtitle">C++</span>
                    <span class="wwd-subtitle">.NET</span>
                </div>
                <div class="col-md-3 text-center">
                    <img width="100" height="100" src="${request.static_url("cdev:static/imgs/ic_web_white_48dp_2x.png")}" alt="cdev"/>
                    <span class="wwd-title">web applications</span>
                    <span class="wwd-subtitle">Front End</span>
                    <span class="wwd-subtitle">Back End</span>
                </div>
                <div class="col-md-3 text-center">
                    <img width="100" height="100" src="${request.static_url("cdev:static/imgs/ic_memory_white_48dp_2x.png")}" alt="cdev"/>
                    <span class="wwd-title">databases</span>
                    <span class="wwd-subtitle">PostgresSQL</span>
                    <span class="wwd-subtitle">SQL Azure</span>
                </div>
                <div class="col-md-3 text-center">
                    <img width="100" height="100" src="${request.static_url("cdev:static/imgs/ic_developer_board_white_48dp_2x.png")}" alt="cdev"/>
                    <span class="wwd-title">web design</span>
                    <span class="wwd-subtitle">UI/UX</span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12 hww-cont">
    <div class="container">
        <div class="col-md-12 text-center">
            <div class="row">
                <div class="hww-text"><span>How we work</span></div>
            </div>
            <div class="row">
                <div class="col-md-12 hww-seq">
                    <div class="col-md-2">
                        <div class="hww-circle">
                            <span>1</span>
                        </div>
                        <div class="hww-line"></div>
                        <div class="hww-title">Collect requirements</div>
                    </div>
                    <div class="col-md-2">
                        <div class="hww-circle">
                            <span>2</span>
                        </div>
                        <div class="hww-line"></div>
                        <div class="hww-title">Establish project required development process</div>
                    </div>
                    <div class="col-md-2">
                        <div class="hww-circle">
                            <span>3</span>
                        </div>
                        <div class="hww-line"></div>
                        <div class="hww-title">Define <br>milestones</div>
                    </div>
                    <div class="col-md-2">
                        <div class="hww-circle">
                            <span>4</span>
                        </div>
                        <div class="hww-line"></div>
                        <div class="hww-title">Provide “access points” for process control from a client side</div>
                    </div>
                    <div class="col-md-2">
                        <div class="hww-circle">
                            <span>5</span>
                        </div>
                        <div class="hww-line"></div>
                        <div class="hww-title">Self-documented code</div>
                    </div>
                    <div class="col-md-2">
                        <div class="hww-circle">
                            <span>6</span>
                        </div>
                        <div class="hww-title">Quality assurance<br> and<br> Support</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-4 col-md-offset-4 hww-lm-btn">
                        <span>learn more</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12 wyg-cont">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center wyg-text">
                <span>what you get?</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5 wyg-title-left"><span id="wyg-sd">Specification document</span></div>
            <div class="col-md-5 col-md-offset-2 wyg-title-left"><span id="wyg-atp">Access to project<br> development tracking tools</span></div>
        </div>
        <div class="row">
            <div class="col-md-5 wyg-list-cont">
                <span>- High level mockups</span>
                <span>- Architecture design and technical solution</span>
                <span>- Defined project scope</span>
                <span>- Detailed estimation</span>
            </div>
            <div class="col-md-5 col-md-offset-2 wyg-list-cont">
                <span>- Tasks progress tracking tool (Jira)</span>
                <span>- Source code storage (Git)</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5 wyg-title-left"><span id="wyg-atp">Access to project development in the middle artifacts </span></div>
            <div class="col-md-5 col-md-offset-2 wyg-title-left"><span id="wyg-sd">Well tested solution</span></div>
        </div>
        <div class="row">
            <div class="col-md-5 wyg-list-cont">
                <span>- Prototype</span>
                <span>- Weekly internal versions</span>
                <span>- Code changes history</span>
                <span>- Weekly reports</span>
            </div>
            <div class="col-md-5 col-md-offset-2 wyg-list-cont">
                <span>- Features testing</span>
                <span>- Integration testing</span>
                <span>- Regression testing</span>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12 wyg-extra-cont">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="col-md-4"><span class="wyg-extra-one">Deployment assistance</span></div>
                <div class="col-md-4"><span class="wyg-extra-two">Free post-release <br> bug fixing period</span></div>
                <div class="col-md-4"><span class="wyg-extra-one">Long term support</span></div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12 consult-cont">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="col-md-10 col-md-offset-1 consult-text">Once per month we run 2 days free consultancy service for one of requests collected for last month. The appropriate company request is chosen on random basis. </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-4 col-md-offset-4" id="consult-btn"><span>Free Consultancy</span></div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12 clients-cont">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <span class="clients-text">OUR CLIENTS</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center clients-logos">
                <div class="col-md-3 vcenter"><img width="190" src="${request.static_url("cdev:static/imgs/kw-logo1.png")}"/></div>
                <div class="col-md-4 vcenter"><img width="190" src="${request.static_url("cdev:static/imgs/vtip.png")}"/></div>
                <div class="col-md-3 vcenter"><img width="300" src="${request.static_url("cdev:static/imgs/Lugert_Slogan_web1.png")}"/></div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12 cform-cont">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <span class="cform-text">GET IN TOUCH WITH US!</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-offset-3 col-md-6 cform-field"><input placeholder="name" id="cform-name" type="text"></div>
            <div class="col-md-offset-3 col-md-6 cform-field"><input placeholder="mail" id="cform-mail" type="text"></div>
            <div class="col-md-offset-3 col-md-6 cform-field"><textarea placeholder="text" id="cform-text"></textarea></div>
            <div class="col-md-12">
                <div class="col-md-2 col-md-offset-5" id="cform-send-btn"><span>send</span></div>
            </div>
        </div>
    </div>
</div>



