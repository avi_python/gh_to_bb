<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CDev</title>
    <link href="${request.static_url("cdev:static/css/bootstrap.min.css")}" rel="stylesheet">
    <link href="${request.static_url("cdev:static/css/base.css")}" rel="stylesheet">
  </head>
  <body>
    <script src="${request.static_url("cdev:static/js/jquery.min.js")}"></script>
    <script src="${request.static_url("cdev:static/js/bootstrap.min.js")}"></script>
    <div class="row" style="margin: 0 0">
        <div class="col-md-12 cdev-navbar navbar-fixed-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-1 logo"><img width="72" height="72" src="${request.static_url("cdev:static/imgs/500 png.png")}"/></div>
                    <div class="col-md-2 m-lnk" id="m-home"><span>HOME</span></div>
                    <div class="col-md-1 m-lnk" id="m-serv"><span>SERVICES</span></div>
                    <div class="col-md-2 m-lnk" id="m-hww"><span>HOW WE WORK</span></div>
                    <div class="col-md-3 m-lnk" id="m-fc"><span>FREE CONSULTANCY</span></div>
                    <div class="col-md-2"><div id="m-contact"><span>contact us</span></div></div>
                    <div class="col-md-1" id="m-map"><img width="48" height="48" src="${request.static_url("cdev:static/imgs/72 Location.png")}"/></div>
                </div>
            </div>
        </div>
      ${ next.body() }
        <div class="col-md-12 cdev-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-1 col-md-3 cdev-footer-menu">
                        <div>Home</div>
                        <div>Services</div>
                        <div>How we work</div>
                        <div>free consultancy</div>
                    </div>
                    <div class="col-md-offset-1 col-md-2 cdev-footer-addr">
                        <div id="cdev-fa">ADDRESS</div>
                        <div id="cdev-loc">
                            7-A Naukova Str.,
                            Office 310,
                            Lviv, Ukraine
                        </div>
                    </div>
                    <div class="col-md-offset-1 col-md-4 cdev-footer-contacts">
                        <div class="cdev-tel"><img src="${request.static_url("cdev:static/imgs/ic_call_white_48dp_1x.png")}"><span>Tel: 099-999-99-99</span></div>
                        <div class="cdev-mail"><img src="${request.static_url("cdev:static/imgs/ic_emai.png")}"><span><a href="mailto:begin@cdev.com.ua">begin@cdev.com.ua</a></span></div>
                        <div class="cdev-skype"><img src="${request.static_url("cdev:static/imgs/sk.png")}"><span><a href="skype:cdev_owner">cdev_owner</a></span></div>
                    </div>
                </div>
            </div>

        </div>


    </div>
    <div class="row" style="margin: 0">
        <div class="col-md-12 text-center cdev-copyright">
            <span>2018 All rights reserved</span>
        </div>
    </div>
  </body>
</html>